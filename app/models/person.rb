class Person < ActiveRecord::Base
  has_many :person_works
  has_many :works, through :person_works
end
