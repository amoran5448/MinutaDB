class Work < ActiveRecord::Base
  has_many :person_works
  has_many :people, through :person_works
  has_many :subjets
end
