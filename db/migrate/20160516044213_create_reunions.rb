class CreateReunions < ActiveRecord::Migration
  def change
    create_table :reunions do |t|
      t.string :subjet
      t.time :time
      t.date :date
      t.string :place

      t.timestamps null: false
    end
  end
end
