class CreateSubjets < ActiveRecord::Migration
  def change
    create_table :subjets do |t|
      t.string :matter_meeting
      t.string :et_meeting
      t.string :agreetments_resolvs
      t.string :work_descriptions
      t.references :agreetments, index: true, foreign_key: true
      t.references :works, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
