class CreatePersonWorks < ActiveRecord::Migration
  def change
    create_table :person_works do |t|
      t.references :works, index: true, foreign_key: true
      t.references :people, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
