class CreateWorks < ActiveRecord::Migration
  def change
    create_table :works do |t|
      t.string :descrip
      t.date :until_date
      t.string :status
      t.string :people_asigned_name
      t.string :people_asigned_lastname
      t.references :people, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
